#passing a single action as input and calculating reward

import glob
import os
import sys
import random
import time
import numpy as np
import cv2
import math

try:
    sys.path.append(glob.glob('C:/Users/pnagaraj/Data/carla_versions/CARLA_0.9.5/PythonAPI/carla/dist/carla-*%d.%d-%s.egg' % (
        sys.version_info.major,
        sys.version_info.minor,
        'win-amd64' if os.name == 'nt' else 'linux-x86_64'))[0])
except IndexError:
    pass
import carla


def get_transform(vehicle_location, angle, d=6.4):
    a = math.radians(angle)
    location = carla.Location(d * math.cos(a), d * math.sin(a), 2.0) + vehicle_location
    return carla.Transform(location, carla.Rotation(yaw=180 + angle, pitch=-15))

class CarEnv:
    SHOW_CAM= True # to view environment
    STEER_AMT= 0.80
    im_width= 640
    im_height = 480
    front_camera = None
    SECONDS_PER_EPISODE = 30

    def __init__(self):
        self.client = carla.Client("localhost",2000) #connect to server
        self.client.set_timeout(2.0)
        self.world = self.client.get_world()
        self.blueprint_library = self.world.get_blueprint_library()
        self.spectator = self.world.get_spectator()


    # begining of the environment, every episode to reset
    def reset(self):
        self.collison_hist = [] #to reset/clear
        self.actor_list  = [] #to reset/clear
        self.list_lane_marking = []


        #vehicle-actor
        self.model_3 = self.blueprint_library.filter("model3")[0] # to get tesla model 3
        self.transform = random.choice(self.world.get_map().get_spawn_points())
        #self.transform = self.start_coord
        self.vehicle = self.world.spawn_actor(self.model_3, self.transform) # passing the model3 bp and the location to spawn
        self.actor_list.append(self.vehicle)


        #sensor
        self.rgb_cam = self.blueprint_library.find('sensor.camera.rgb')
        self.rgb_cam.set_attribute("image_size_x", f"{self.im_width}")
        self.rgb_cam.set_attribute("image_size_y", f"{self.im_height}")
        self.rgb_cam.set_attribute("fov", f"120")
        transform = carla.Transform(carla.Location(x=2.5, z=0.7))
        self.sensor = self.world.spawn_actor(self.rgb_cam, transform, attach_to = self.vehicle)
        self.actor_list.append(self.sensor)
        self.sensor.listen(lambda data: self.process_img(data))

        self.vehicle.apply_control(carla.VehicleControl(throttle= 0.0, brake = 0.0))
        time.sleep(4)

        #collision sensor
        colsensor = self.blueprint_library.find("sensor.other.collision")
        self.colsensor = self.world.spawn_actor(colsensor, transform, attach_to = self.vehicle)
        self.actor_list.append(self.colsensor)
        self.colsensor.listen(lambda event: self.collision_data(event)) #just record the collision events

        #lane invasion sensor
        lane_invasion = self.blueprint_library.find("sensor.other.lane_invasion")
        self.lanesensor = self.world.spawn_actor(lane_invasion, transform, attach_to = self.vehicle)
        self.actor_list.append(self.lanesensor)
        self.lanesensor.listen(lambda events: self.laneprocess(events))



        while self.front_camera is None:
            time.sleep(0.01)

        self.episode_start = time.time() # save the start time of the episode
        self.vehicle.apply_control(carla.VehicleControl(throttle= 0.0, brake= 0.0))
        angle = 90
        while angle < 300:
            timestamp = self.world.wait_for_tick()
            angle += timestamp.delta_seconds * 60.0
            self.spectator.set_transform(get_transform(self.actor_list[0].get_location(), angle - 90))


        return self.front_camera

    def collision_data(self,event):
        self.collison_hist.append(event)

    def laneprocess(self,events):
        self.list_lane_marking.append(events)


    def process_img(self, image):
        i=np.array(image.raw_data)
        i2= i.reshape((self.im_height,self.im_width,4))
        i3= i2[:,:,:3]
        if self.SHOW_CAM: #if true it will show thw images captured by camera
            cv2.imshow("",i3)
            cv2.waitKey(1)
        self.front_camera =i3

    def step(self, action):
        if action == 0:
            self.vehicle.apply_control(carla.VehicleControl(throttle = 1.0, steer= 0) )#go straight
        if action == 1:
            self.vehicle.apply_control(carla.VehicleControl(throttle = 1.0, steer= -1 * self.STEER_AMT)) #go left
        if action == 2:
            self.vehicle.apply_control(carla.VehicleControl(throttle = 1.0, steer= 1 * self.STEER_AMT)) #go right
        if action == 3:
            self.vehicle.apply_control(carla.VehicleControl(throttle = 0.0, steer = 0)) #no_action
        if action == 4:
            self.vehicle.apply_control(carla.VehicleControl(throttle = 0.0, steer = 0, brake = 1.0)) #brake
        if action == 5:
            self.vehicle.apply_control(carla.VehicleControl(throttle = 0.0, steer = -1 * self.STEER_AMT, brake = 0.002)) #brakeandturnleft
        if action == 6:
            self.vehicle.apply_control(carla.VehicleControl(throttle = 0.0, steer = 1 * self.STEER_AMT, brake = 1.0)) #brakeandturnright

        v = self.vehicle.get_velocity()
        kmh = int(3.6 * math.sqrt(v.x**2 + v.y**2 + v.z**2)) # converting velocity to km per hour
        #print(f"velocity: {v}")
        print(f"kmh:{kmh}")

        #reward calculation based on collision, speed
        if len(self.collison_hist)!= 0:  #any collison terminate the episode
            done= True
            reward = -1 #-200

        if len(self.list_lane_marking) > 0:
            done = False
            print("lane penalty")
            reward = -1

        elif kmh< 50: #speed # low speed, gets penalty
            done = False
            reward = -0.005 #-1

        else:
            done = False
            reward = 0.005 #1

        if self.episode_start + self.SECONDS_PER_EPISODE < time.time(): # if episode doesnt complete within 10 seconds, then terminate the episode
            done = True


        return self.front_camera, reward, done, None


if __name__ == '__main__':

    env= CarEnv()
    env.reset()
    done= False
    total_reward = 0
    try:
        for i in range(1):
            while not done:
                cam, reward, done, _ = env.step(0)
                total_reward += reward
                print(reward)
                time.sleep(0.3)
            print("total reward per episode",total_reward)
    finally:
        for actor in env.actor_list:
            actor.destroy()
            
