from collections import deque
from keras.layers import Dense,Conv2D, Activation,AveragePooling2D, Flatten
from keras.optimizers import Adam
from keras.callbacks import TensorBoard
from keras.models import Sequential
import tensorflow as tf
import keras.backend.tensorflow_backend as backend
from threading import Thread
from tqdm import tqdm
import os
import numpy as np
import random
import time
#from Carla_environment.Carlaenv_gym import *
from rl_dqn_for_scenario.Carla_environment.Carlaenv import *


os.environ['TF_CPP_MIN_LOG_LEVEL']='3'
REPLAY_MEMORY_SIZE = 5000
MIN_REPLAY_REPLAY_MEMORY_SIZE = 2048 #1000
MINIBATCH_SIZE = 16
PREDICTION_BATCH_SIZE = 1
TRAINING_BATCH_SIZE = MINIBATCH_SIZE //4
UPDATE_TARGET_EVERY = 5 #updating target model after 5 episodes
MODEL_NAME = "new_train"
# amount of memory consumption
MEMORY_FRACTION = 0.1
MIN_REWARD = -200
EPISODES = 10000
DISCOUNT = 0.99
epsilon = 1
EPSILON_DECAY = 0.9 #0.95 0.9975 0.99975
MIN_EPSILON = 0.1
AGGREGATE_STATS_EVERY = 10
X = []
Y = []


class ModifiedTensorBoard(TensorBoard):

    # Overriding init to set initial step and writer (we want one log file for all .fit() calls)
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.step = 1
        self.writer = tf.summary.FileWriter(self.log_dir)

    # Overriding this method to stop creating default log writer
    def set_model(self, model):
        pass

    # Overrided, saves logs with our step number
    # (otherwise every .fit() will start writing from 0th step)
    def on_epoch_end(self, epoch, logs=None):
        self.update_stats(**logs)

    # Overrided
    # We train for one batch only, no need to save anything at epoch end
    def on_batch_end(self, batch, logs=None):
        pass

    # Overrided, so won't close writer
    def on_train_end(self, _):
        pass

    # Custom method for saving own metrics
    # Creates writer, writes custom metrics and closes writer
    def update_stats(self, **stats):
        self._write_logs(stats, self.step)

#class for RL agent --> control for Rl agent
class DQNAgent:
    def __init__(self):
        #INITIALIZATION: replay memory, a-v function q with random weights Ö, target a-v function q' with weights Ö' = Ö
        #training model
        self.model = self.create_model()
        #target model updating every step
        self.target_model = self.create_model()
        self.target_model.set_weights(self.model.get_weights())
        #memory of size 5000
        self.replay_memory = deque(maxlen= REPLAY_MEMORY_SIZE) #deque allows adding and removing elements-memory of previous actions
        #to save logs in folder logs/Xception-time
        self.tensorboard = ModifiedTensorBoard(log_dir = f"logs/{MODEL_NAME}- {int(time.time())}")
        self.target_update_counter = 0
        self.graph = tf.get_default_graph()

        self.terminate = False
        self.last_logged_episode = 0
        self.training_initialized = False

        #model can be changed
    def create_model(self): #takes state as input and returns q_values for all action

        model = Sequential()
        model.add(Conv2D(64,(3,3), input_shape= (IM_HEIGHT, IM_WIDTH, 3)))
        model.add(Activation('relu'))
        model.add(AveragePooling2D(pool_size=(5, 5), strides=(3, 3), padding='same'))

        model.add(Conv2D(64, (3, 3), padding='same'))
        model.add(Activation('relu'))
        model.add(AveragePooling2D(pool_size=(5, 5), strides=(3, 3), padding='same'))

        model.add(Conv2D(64, (3, 3), padding='same'))
        model.add(Activation('relu'))
        model.add(AveragePooling2D(pool_size=(5, 5), strides=(3, 3), padding='same'))

        model.add(Flatten())

        model.add(Dense(num_actions, activation = "linear"))
        model.compile(loss="mse", optimizer=Adam(lr=0.0001), metrics = ['accuracy']) #lr = 0.001
        return model

    # saving all the transitions in replay memory- transitions required to train model
    # transition = (current_state, action, reward, new_state, done)
    def update_replay_memory(self, transition):
        return self.replay_memory.append(transition)

    def train(self):
        if len(self.replay_memory)< MIN_REPLAY_REPLAY_MEMORY_SIZE: #1000
            return

        #print(f"\n replay_memory_size: {len(self.replay_memory)}")
        minibatch = random.sample(self.replay_memory, MINIBATCH_SIZE) #(memory,16)-select random 16 samples
        current_states = np.array([transition[0] for transition in minibatch])/255 #current state is image so /255 normalizing
        with self.graph.as_default():
            #given current_state i.e the image as input and batch_size =1, it gives output action q value
            #predicting for the recorded transition
            current_qs_list = self.model.predict(current_states, PREDICTION_BATCH_SIZE)
            #print("current_qs_list: actions::",current_qs_list) #outputs q_values of actions

        new_current_states = np.array([transition[3] for transition in minibatch])/255
        with self.graph.as_default():
            future_qs_list = self.target_model.predict(new_current_states, PREDICTION_BATCH_SIZE)


        #finidng q value for each state using equation(new_q = reward+ discount*max(future_q)
        for index, (current_state, action, reward, new_state, obs) in enumerate(minibatch):
            if not self.done:
               # max_future_q = np.max(future_qs_list[index]) #q_value
                new_q = reward + DISCOUNT * np.max(future_qs_list[index]) #target network to calculate q_future
            else:
                new_q = reward

            current_qs = current_qs_list[index]
            current_qs[action]= new_q #assigning the computed q value to the action taken from current_state

            X.append(current_state) #current state from replay
            Y.append(current_qs) #computed q_value using equation

        #values for tensorboard
        log_this_step = False
        if self.tensorboard.step > self.last_logged_episode: #(1>0)
            log_this_step = True
            self.last_logged_episode = self.tensorboard.step

        with self.graph.as_default():#batch-size =4
            # finding loss_q_learning update:: (equation q_value new_q and q_value from network for current state
            self.model.fit(np.array(X)/255, np.array(Y), batch_size = TRAINING_BATCH_SIZE, verbose = 0, shuffle = False, callbacks =[self.tensorboard] if log_this_step else None)

        if log_this_step:
            self.target_update_counter +=1 #used to log for every 5 episodes

        if self.target_update_counter > UPDATE_TARGET_EVERY:
            self.target_model.set_weights(self.model.get_weights())
            self.target_update_counter = 0

    def get_qs(self, state): #returns
        print(self.model.predict(np.array(state).reshape(-1, *state.shape)/255)[0])
        return self.model.predict(np.array(state).reshape(-1, *state.shape)/255)[0] #unpacking h*w*3 from state

    def train_in_loop(self):
        #fit for nothing, like not real data
        X = np.random.uniform(size = (1, IM_HEIGHT, IM_WIDTH, 3)).astype(np.float32) #random array of size(640,480,3)
        Y = np.random.uniform(size =(1,num_actions)).astype(np.float32) #random 3 numbers [[va1,val2,val3]]
        with self.graph.as_default():
            #training. X-input, Y-output
            self.model.fit(X,Y, verbose = False, batch_size =1)

        self.training_initialized = True

        while True:
            if self.terminate:
                return
            self.train()
            time.sleep(0.01)

#if __name__ =='__main__':
class Traindqn():
    def __init__(self, actor):


        self.FPS = 40
        self.ep_rewards = [-10]
        self.actor = actor
        #for repetability
        random.seed(1) #generate same random number
        np.random.seed(1)
        tf.set_random_seed(1)#To make the random sequences generated by all ops be repeatable across sessions, set a graph-level seed:

        #incase multiple agents:::
        gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction = MEMORY_FRACTION)
        #Sets the global TensorFlow session.
        backend.set_session(tf.Session(config=tf.ConfigProto(gpu_options = gpu_options)))

        if not os.path.isdir("models"):
            os.makedirs("models")
    def run(self):
        print('runnning')
        agent = DQNAgent()
        env = CarEnv(self.actor)

        #Using threads allows a program to run multiple operations concurrently in the same process space.
        #instantiating thread with target function and call start() to begin working, daemon doesnt have to wait for all threads to finish
        trainer_thread = Thread(target = agent.train_in_loop, daemon = True) #train and predict simultaneosuly
        trainer_thread.start()

        #until agent.training_init is True it waits
        while not agent.training_initialized:
            time.sleep(0.01)

        agent.get_qs(np.ones((env.im_height,env.im_width, 3)))

        """DURING EPISODE; updating REPLAY MEM, predict and train """
        for episode in tqdm(range(1, EPISODES +1), ascii= True, unit = "episodes"): #ascii and unit are for display w.r.t tqdm
            agent.tensorboard.step = episode
            episode_reward = 0
            step = 1
            current_state = env.reset()
            self.done = False
            episode_start = time.time()

            while True: #while not done
                if np.random.random()> epsilon:
                    action = np.argmax(agent.get_qs(current_state)) #exploit #get_qs returns probability of all actions, one with max value is chosen
                else:
                    action = np.random.randint(0,num_actions) #explore
                    time.sleep(1/self.FPS)

                new_state, reward, done, _ = env.step(action)
                episode_reward += reward

                #is current state always reset??
                agent.update_replay_memory((current_state, action, reward, new_state, done)) #adding each transition to the replay-memory of size 5000

                step += 1

                if done:
                    print("episode:{}_reward:{}".format(episode,episode_reward))
                    break

            for actor in env.actor_list:
                actor.destroy()

            #*****************************************************************************#
            # Append episode reward to a list and log stats (every given number of episodes)
            self.ep_rewards.append(episode_reward)
            if not episode % AGGREGATE_STATS_EVERY or episode == 1:
                average_reward = sum(self.ep_rewards[-AGGREGATE_STATS_EVERY:])/len(self.ep_rewards[-AGGREGATE_STATS_EVERY:])
                min_reward = min(self.ep_rewards[-AGGREGATE_STATS_EVERY:])
                max_reward = max(self.ep_rewards[-AGGREGATE_STATS_EVERY:])
                agent.tensorboard.update_stats(reward_avg=average_reward, reward_min=min_reward, reward_max=max_reward, epsilon=epsilon)

                # Save model, but only when min reward is greater or equal a set value
                if average_reward >= MIN_REWARD: #can change to avg model or whatever we need
                    print('saving model')
                    agent.model.save(f'models/{MODEL_NAME}__{max_reward:_>7.2f}max_{average_reward:_>7.2f}avg_{min_reward:_>7.2f}min__{int(time.time())}.model')

            # Decay epsilon
            if epsilon > MIN_EPSILON:
                epsilon *= EPSILON_DECAY
                epsilon = max(MIN_EPSILON, epsilon)

        # Set termination flag for training thread and wait for it to finish
        agent.terminate = True
        trainer_thread.join()
        #agent.model.save(f'models/{MODEL_NAME}_{max_reward:_>7.2f}max_{average_reward:_>7.2f}avg_{min_reward::_>7.2f}min_{int(time.time())}.model')
        print('saving model_train')
        agent.model.save(f'models/{MODEL_NAME}_{max_reward}max_{average_reward}avg_{min_reward}min_{int(time.time())}.model')