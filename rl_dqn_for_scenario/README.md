Carla version = 0.9.5

* Here, **dqn_eval.py** is the evaluation code--> testing code
* The trained models are saved in **models** folder
* **Carla_environmnet/Carlaenv.py** is the carla environment file
* **dqn_myconv.py** is the code for dqn (dqn_simpleconv, dqn_xception, dqn_autonomous are dqn code with different networks)

1.  Place both **Rl_dqn_for_scenario** and **scenario_runner_modified** in carla-->PythonAPI-->examples folder
2.  Run python *scenario_runner.py --scenario FollowLeadingVehicle_1*
3.  Run *python manual_control.py -a*



*  If anaconda is used -->
*  conda create --name myenv python=3.7.6
*  conda install -c conda-forge tensorflow=1.13.1
*  conda install -c conda-forge keras
*  conda install -c conda-forge opencv
