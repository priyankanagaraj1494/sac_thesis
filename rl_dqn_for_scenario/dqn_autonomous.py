from collections import deque
from keras.layers import Dense,Conv2D, Dropout,MaxPooling2D, Flatten, Input
from keras.optimizers import Adam
from keras.callbacks import TensorBoard
from keras.models import Model
from keras.initializers import random_normal
import tensorflow as tf
import keras.backend.tensorflow_backend as backend
from threading import Thread
import logging
import time
from tqdm import tqdm
from PythonAPI.examples.Rl_carla_win.Carla_environment.Carlaenv_gym import *
#from PythonAPI.examples.Rl_carla_win.Carla_environment.Carlaenv import *


os.environ['TF_CPP_MIN_LOG_LEVEL']='3'
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
REPLAY_MEMORY_SIZE = 2000 #5000
MIN_REPLAY_REPLAY_MEMORY_SIZE =  1000 #2048
MINIBATCH_SIZE = 32 #16
PREDICTION_BATCH_SIZE = 1
TRAINING_BATCH_SIZE = MINIBATCH_SIZE //4
UPDATE_TARGET_EVERY = 300 #how often the weights from the actively trained network get copied to the target network
MODEL_NAME = "conv"
# amount of memory consumption
MEMORY_FRACTION = 0.1
MIN_REWARD = -50
EPISODES = 10000
DISCOUNT = 0.99
epsilon = 1
EPSILON_DECAY = 0.003 #0.95 0.9975 0.99975 0.9
MIN_EPSILON = 0.1
AGGREGATE_STATS_EVERY = 10
X = []
Y = []


class ModifiedTensorBoard(TensorBoard):

    # Overriding init to set initial step and writer (we want one log file for all .fit() calls)
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.step = 1
        self.writer = tf.summary.FileWriter(self.log_dir)

    # Overriding this method to stop creating default log writer
    def set_model(self, model):
        pass

    # Overrided, saves logs with our step number
    # (otherwise every .fit() will start writing from 0th step)
    def on_epoch_end(self, epoch, logs=None):
        self.update_stats(**logs)

    # Overrided
    # We train for one batch only, no need to save anything at epoch end
    def on_batch_end(self, batch, logs=None):
        pass

    # Overrided, so won't close writer
    def on_train_end(self, _):
        pass

    # Custom method for saving own metrics
    # Creates writer, writes custom metrics and closes writer
    def update_stats(self, **stats):
        self._write_logs(stats, self.step)

#class for RL agent --> control for Rl agent
class DQNAgent:
    def __init__(self):
        #INITIALIZATION: replay memory, a-v function q with random weights Ö, target a-v function q' with weights Ö' = Ö
        #training model
        self.model = self.create_model()
        #target model updating every step
        self.target_model = self.create_model()
        self.target_model.set_weights(self.model.get_weights())
        #memory of size 5000
        self.replay_memory = deque(maxlen= REPLAY_MEMORY_SIZE) #deque allows adding and removing elements-memory of previous actions
        #to save logs in folder logs/Xception-time
        self.tensorboard = ModifiedTensorBoard(log_dir = f"logs/{MODEL_NAME}- {int(time.time())}")
        self.target_update_counter = 0
        self.graph = tf.get_default_graph()

        self.terminate = False
        self.last_logged_episode = 0
        self.training_initialized = False

    def create_model(self): #takes state as input and returns q_values for all action

        activation = 'relu'
        # The main model input.
        pic_input = Input(shape=(IM_HEIGHT, IM_WIDTH, 3))
        train_conv_layers = False  # For transfer learning, set to True if training ground up.

        img_stack = Conv2D(16, (3, 3), name='convolution0', padding='same', activation=activation,
                           trainable=train_conv_layers)(pic_input)
        img_stack = MaxPooling2D(pool_size=(2, 2))(img_stack)
        img_stack = Conv2D(32, (3, 3), activation=activation, padding='same', name='convolution1',
                           trainable=train_conv_layers)(img_stack)
        img_stack = MaxPooling2D(pool_size=(2, 2))(img_stack)
        img_stack = Conv2D(32, (3, 3), activation=activation, padding='same', name='convolution2',
                           trainable=train_conv_layers)(img_stack)
        img_stack = MaxPooling2D(pool_size=(2, 2))(img_stack)
        img_stack = Flatten()(img_stack)
        img_stack = Dropout(rate = 0.2)(img_stack)

        img_stack = Dense(128, name='rl_dense', kernel_initializer=random_normal(stddev=0.01))(img_stack)
        img_stack = Dropout(rate =0.2)(img_stack)
        output = Dense(num_actions, name='rl_output', kernel_initializer=random_normal(stddev=0.01))(img_stack)

        opt = Adam()
        action_model = Model(inputs=[pic_input], outputs=output)

        action_model.compile(optimizer=opt, loss='mean_squared_error')# it is regression
        #action_model.summary()
        return action_model


    # saving all the transitions in replay memory- transitions required to train model
    # transition = (current_state, action, reward, new_state, done)
    def update_replay_memory(self, transition):
        return self.replay_memory.append(transition)

    def train(self):
        if len(self.replay_memory)< MIN_REPLAY_REPLAY_MEMORY_SIZE: #1000
            return

        minibatch = random.sample(self.replay_memory, MINIBATCH_SIZE) #(memory,16)-select random 16 samples
        current_states = np.array([transition[0] for transition in minibatch])/255 #current state is image so /255 normalizing
        with self.graph.as_default():
            current_qs_list = self.model.predict(current_states, PREDICTION_BATCH_SIZE)
        new_current_states = np.array([transition[3] for transition in minibatch])/255
        with self.graph.as_default():
            future_qs_list = self.target_model.predict(new_current_states, PREDICTION_BATCH_SIZE)

        for index, (current_state, action, reward, new_state, obs) in enumerate(minibatch):
            if not done:
                max_future_q = np.max(future_qs_list[index]) #q_value
                new_q = reward + DISCOUNT * max_future_q
            else:
                new_q = reward

            current_qs = current_qs_list[index]
            current_qs[action]= new_q

            X.append(current_state)
            Y.append(current_qs)

        #values for tensorboard
        log_this_step = False
        if self.tensorboard.step > self.last_logged_episode:
            log_this_step = True
            self.last_logged_episode = self.tensorboard.step

        with self.graph.as_default():
            # finding loss_q_learning update:: (equation q_value new_q and q_value from network for current state
            np.resize(X, (-1, X.shape))
            self.model.fit(np.array(X)/255, np.array(Y), batch_size = TRAINING_BATCH_SIZE, verbose = 0, shuffle = False, callbacks =[self.tensorboard] if log_this_step else None)

        if log_this_step:
            self.target_update_counter +=1 #used to log for every 5 episodes

        if self.target_update_counter > UPDATE_TARGET_EVERY:
            self.target_model.set_weights(self.model.get_weights())
            self.target_update_counter = 0

    def get_qs(self, state):
        return self.model.predict(np.array(state).reshape(-1, *state.shape)/255)[0] #unpacking h*w*3 from state

    def train_in_loop(self):
        #fit for nothing, like not real data
        #X = np.random.uniform(size = (1, IM_HEIGHT, IM_WIDTH, 3)).astype(np.float32) #random array of size(640,480,3)
        #Y = np.random.uniform(size =(1,num_actions)).astype(np.float32) #random 3 numbers [[va1,val2,val3]]
        #with self.graph.as_default():
         #   self.model.fit(X,Y, verbose = False, batch_size =1)

        self.training_initialized = True

        while True:
            if self.terminate:
                return
            self.train()
            time.sleep(0.01)

if __name__ =='__main__':

    FPS = 40
    ep_rewards = [-10]
    random.seed(1)
    np.random.seed(1)
    tf.set_random_seed(1)

    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction = MEMORY_FRACTION)
    backend.set_session(tf.Session(config=tf.ConfigProto(gpu_options = gpu_options)))
    if not os.path.isdir("models"):
        os.makedirs("models")

    agent = DQNAgent()
    env = CarEnv()
    logging.basicConfig(format="%(asctime)s: %(message)s", level = logging.INFO, datefmt="%H.%M.%S")
    logging.info("Thread: train started")
    trainer_thread = Thread(target = agent.train_in_loop, daemon = True) #train and predict simultaneosuly
    trainer_thread.start()

    #until agent.training_init is True it waits
    while not agent.training_initialized:
        time.sleep(0.01)

    agent.get_qs(np.ones((env.im_height,env.im_width, 3)))

    """DURING EPISODE; updating REPLAY MEM, predict and train """
    for episode in tqdm(range(1, EPISODES +1), ascii= True, unit = "episodes"): #ascii and unit are for display w.r.t tqdm
        agent.tensorboard.step = episode
        episode_reward = 0
        step = 1
        current_state = env.reset()
        done = False
        episode_start = time.time()

        while True: #while not done
            if np.random.random()> epsilon:
                action = np.argmax(agent.get_qs(current_state)) #exploit
            else:
                action = np.random.randint(0,num_actions) #explore
                time.sleep(1/FPS)

            new_state, reward, done, _ = env.step(action)
            episode_reward += reward

            #is current state always reset??
            agent.update_replay_memory((current_state, action, reward, new_state, done)) #adding each transition to the replay-memory of size 5000

            step += 1

            if done:
                print("episode:{}_reward:{}".format(episode,episode_reward))
                break

        for actor in env.actor_list:
            actor.destroy()

        #*****************************************************************************#
        # Append episode reward to a list and log stats (every given number of episodes)
        ep_rewards.append(episode_reward)
        if not episode % AGGREGATE_STATS_EVERY or episode == 1:
            average_reward = sum(ep_rewards[-AGGREGATE_STATS_EVERY:])/len(ep_rewards[-AGGREGATE_STATS_EVERY:])
            min_reward = min(ep_rewards[-AGGREGATE_STATS_EVERY:])
            max_reward = max(ep_rewards[-AGGREGATE_STATS_EVERY:])
            agent.tensorboard.update_stats(reward_avg=average_reward, reward_min=min_reward, reward_max=max_reward, epsilon=epsilon)

            # Save model, but only when min reward is greater or equal a set value
            if average_reward >= MIN_REWARD: #can change to avg model or whatever we need
                agent.model.save(f'models/{MODEL_NAME}__{max_reward:_>7.2f}max_{average_reward:_>7.2f}avg_{min_reward:_>7.2f}min__{int(time.time())}.model')

        # Decay epsilon
        if epsilon > MIN_EPSILON:
            epsilon *= EPSILON_DECAY
            epsilon = max(MIN_EPSILON, epsilon)

    # Set termination flag for training thread and wait for it to finish
    agent.terminate = True
    trainer_thread.join()
    #agent.model.save(f'models/{MODEL_NAME}_{max_reward:_>7.2f}max_{average_reward:_>7.2f}avg_{min_reward::_>7.2f}min_{int(time.time())}.model')
    agent.model.save(f'models/{MODEL_NAME}_{max_reward}max_{average_reward}avg_{min_reward}min_{int(time.time())}.model')